#!/usr/bin/perl

use 5.14.0;
use DateTime;


my @output_files;
for my $arg (@ARGV){
	opendir (DIR, $arg) or push @output_files,$arg;
	while (my $file = readdir(DIR)){
		if (not($file =~ /^\.$/ or  $file =~ /^\.\.$/)){
			push @output_files,"$arg/$file";
		}
	}
}

open(file, "<", "test-input") or die "can't open file";
open(output, ">>", shift @output_files);

my %system_status;
my $system_start_counter = 0;
while (<file>){
	chomp;
	if ((m/\bSYSTEM STOP$/i or m/SYSTEM START$/i)){
		end_of_session();
		if (/START/) {
			++$system_start_counter;
			while (not(-w output)){
				open (output, ">>", shift @output_files);
			}
			die "end of list of output files\n" unless -w output;
			print output "System start $system_start_counter:\n";
		}
		next;
	}
	my($day,$month,$year,$hour,$minute,$second, $object_name, $task, $status) = m/(\d{2})\.(\d{2})\.(\d{4})\s+(\d{2}):(\d{2}):(\d{2})\s+(.+):\s+(\w+)\s+(\w+)/x;
	my $dt = DateTime->new(
		year => $year,
		month => $month,
		day => $day,
		hour => $hour,
		minute => $minute,
		second => $second,
	);
	if (exists $system_status{$object_name}){
		if (($system_status{$object_name}->{task} eq $task) and ($system_status{$object_name}->{status} eq "STARTED") and ($status eq "COMPLETE")){
			my $duration = $dt->delta_ms($system_status{$object_name}->{date});
			$system_status{$object_name}->{log} .= "$object_name: $task COMPLETE";
			$system_status{$object_name}->{log} .= " $duration->{minutes} minutes and" if $duration->{minutes} > 0;
			$system_status{$object_name}->{log} .= " $duration->{seconds} seconds\n";
		}
		elsif (($system_status{$object_name}->{task} eq $task) and (not ($status eq "COMPLETE"))) {
			$system_status{$object_name}->{log} .= "$object_name: ".$system_status{$object_name}->{task} ." didn't end, but $task is $status\n";
		}
		elsif (($system_status{$object_name}->{task} eq $task) and ($status eq "COMPLETE")) {
			$system_status{$object_name}->{log} .= "$object_name: ".$system_status{$object_name}->{task} ." didn't started but complete\n";
		}
		elsif ($system_status{$object_name}->{task} eq "STARTED"){
			$system_status{$object_name}->{log} .= "$object_name: ".$system_status{$object_name}->{task} ." didn't end, but $task is $status\n";
		}
	}
	
				
		$system_status{$object_name}->{task} = $task;
		$system_status{$object_name}->{status} = $status;
		$system_status{$object_name}->{date} = $dt;
	
	
}
end_of_session();


sub end_of_session{
	for my $key (keys %system_status){
		if ($system_status{$key}->{status} eq "STARTED"){
			$system_status{$key}->{log} .= "$key: " . $system_status{$key}->{task} . " didn't end\n"
		}	
	}
	for my $key (sort(keys %system_status)){
		print output $system_status{$key}->{log};
	}
	%system_status = undef;	
}
